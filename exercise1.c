/** This program takes a number from the user and checks 
 *  whether that number is either positive or negative or zero.
 */

#include <stdio.h>

int main()
{
    int number;
    printf("Enter a number : ");
    scanf("%d", &number);

    if (number > 0)
    {
        printf("Number is positive\n");
    }
    else if (number == 0)
    {
        printf("Number is zero\n");
    }
    else
    {
        printf("Number is negative\n");
    }

    return 0;
}