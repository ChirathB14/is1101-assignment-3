/** This program takes a number from the user and checks whether 
 *  that number is odd or even.
 */

#include <stdio.h>
int main()
{
    int number;
    printf("Enter a number : ");
    scanf("%d", &number);

    if (number % 2 == 0)
    {
        printf("Entered number is even!\n");
    }
    else
    {
        printf("Entered number is odd!\n");
    }

    return 0;
}
