/** This program check a given character is Vowel or Consonant(works for both capital and simple letters)
 *  It will also check whether user entered a letter.
 */
#include <stdio.h>
int main()
{
    char input;

    printf("Enter a letter :");
    scanf("%c", &input);

    /*
     *  This if statement checks whether the user has entered a letter by
     *  getting the ASCII value of the character entered by the user
     */
    if (((int)input >= 65 && (int)input <= 90) ||
        ((int)input >= 97 && (int)input <= 122))
    {
        /*
         * This if block check whether the user has entered a vowel
         */
        if (input == 'a' || input == 'e' || input == 'i' || input == 'o' || input == 'u' 
            || input == 'A' || input == 'E' || input == 'I' || input == 'O' || input == 'U'
           )
        {
            printf("%c is a vowel!\n", input);
        }
        else
        {
            printf("%c is a consonant!\n", input);
        }
    }
    else
    {
        printf("please enter a letter!");
    }
    return 0;
}
